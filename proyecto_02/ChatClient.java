import java.net.*;
import java.io.*;
import java.util.concurrent.TimeUnit;

public class ChatClient {

	private String serverName;
	private int serverPort;
	private Socket socket;
	private DataInputStream  keyBoard; 
	private DataInputStream bufferIn; 
    private DataOutputStream bufferOut; 

	public ChatClient(String _serverName, int _serverPort){
		this.serverName = _serverName;
		this.serverPort = _serverPort;
		instantiateSocket();
		sender();
	}

	private void instantiateSocket(){
		try{
			this.socket = new Socket(this.serverName,this.serverPort);
		}catch (Exception e) {
			System.out.println("Unknown Host");			
		}
	}
	private void sender(){

		bindOutputBuffers();

        String outputText = ""; 
        String inputText = ""; 
  
        while (!outputText.equals(".bye")) 
        { 
            try
            { 
                System.out.print("YOU: ");
                outputText = keyBoard.readLine(); 
                long startTime = System.currentTimeMillis();
                bufferOut.writeUTF(outputText); 
                inputText = bufferIn.readUTF(); 
                long endTime = System.currentTimeMillis();
                long elapsedTime = endTime-startTime;
                System.out.println("\t\tServer time:        " + inputText); 
                System.out.println("\t\tRound trip time:    " + 
                    elapsedTime); 
                System.out.println("\t\tActual client time: " + 
                    java.time.LocalDateTime.now()); 
                System.out.println("\t\tSync client time:   " + "do operation"); 
            } 
            catch(IOException i) 
            { 
                System.out.println(i); 
            } 
        } 

        closeBuffers();

	}

	private void bindOutputBuffers(){
		try{
            keyBoard = new DataInputStream(System.in); 
            bufferIn = new DataInputStream( 
                		new BufferedInputStream(
                			socket.getInputStream()
            			)
        			); 
            bufferOut = new DataOutputStream(socket.getOutputStream()); 

		}catch (Exception e) {
			
		}
	}

	private void closeBuffers(){
        try
        { 
            keyBoard.close(); 
            bufferOut.close(); 
            socket.close(); 
        } 
        catch(IOException i) 
        { 
            System.out.println(i); 
        } 
	}

	public static void main(String[] args) {
		ChatClient cc = new ChatClient("localhost",5000);
	}
}
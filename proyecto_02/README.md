#Java simple chat

ChatSever and ChatClient implementation using sockets in java.

## Compilation

**Server**
javac ChatServer.java

**ChatClient**
javac ChatClient.java

## Usage

In a terminal run the server: java ChatServer,
in **another** terminal run the client: java ChatClient.

You can run as many clients as you like.

After the client is execute, write something :p 

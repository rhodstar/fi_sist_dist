import java.net.*;
import java.io.*;

public class ChatServer {

	private int port;
	private Socket socket;
	private ServerSocket server ;
	private DataInputStream bufferIn; 
	private DataOutputStream bufferOut; 
	private DataInputStream  keyBoard; 

	public ChatServer(int _port){
		this.port = _port;
		instantiateSocket();
		listener();
	}

	private void instantiateSocket(){
		try{
			server = new ServerSocket(this.port); 
            System.out.println("Server started"); 
            System.out.println("Waiting for a client ...");
		}catch(IOException ioe){  
        	System.out.println("Can not bind to port " + this.port); 
      }
	}

	private void listener(){
		
		bindInputBuffer();

		String inputText = ""; 

        while (!inputText.equals(".bye")) 
        { 
            try
            { 
                inputText = bufferIn.readUTF(); 
                System.out.println("Client says: " + inputText); 
                bufferOut.writeUTF(java.time.LocalDateTime.now().
                	format(java.time.format.DateTimeFormatter.
                		ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));

            } 
            catch(IOException i) 
            { 
                System.out.println(i); 
            } 
        } 

        closeBuffers();

	}

	public void bindInputBuffer(){
		try{
			socket = server.accept(); 
  			System.out.println("\n\n========================");
            System.out.println("Client accepted"); 
  			System.out.println("========================\n\n");
            bufferIn = new DataInputStream( 
                		new BufferedInputStream(
                			socket.getInputStream()
            			)
        			); 
            bufferOut = new DataOutputStream(socket.getOutputStream()); 
		}catch (Exception e) {
			
		}
	}

	public void closeBuffers(){
		try{
            bufferIn.close(); 
            socket.close(); 
		}catch (Exception e) {
			
		}
	}

	public static void main(String[] args) {
		ChatServer cs = new ChatServer(5000);
	}
}
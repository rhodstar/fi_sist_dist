<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'api\RegisterController@register');
Route::post('login', 'api\RegisterController@login');
Route::get('user/cuenta/consultar','api\CuentaBancariaController@showSaldo');
Route::post('user/cuenta/actualizar','api\CuentaBancariaController@updateSaldo');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

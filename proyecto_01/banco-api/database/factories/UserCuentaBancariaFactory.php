<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\UserCuentaBancaria::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1,20),
        'cuenta_bancaria_id'=>  $faker->numberBetween(1,20)
    ];
});

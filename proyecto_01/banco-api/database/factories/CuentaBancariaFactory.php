<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\CuentaBancaria::class, function (Faker $faker) {
    return [
        'fecha_apertura' => $faker->date('Y-m-d'),
        'fecha_consulta' => $faker->dateTime(),
        'saldo' => $faker->randomFloat(2,1000.00,10000.00),
    ];
});

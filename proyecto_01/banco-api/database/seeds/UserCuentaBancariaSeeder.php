<?php

use Illuminate\Database\Seeder;

class UserCuentaBancariaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\UserCuentaBancaria::class,60)->create();
    }
}

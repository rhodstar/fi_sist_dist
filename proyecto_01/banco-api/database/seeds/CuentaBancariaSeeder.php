<?php

use Illuminate\Database\Seeder;

class CuentaBancariaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\CuentaBancaria::class,20)->create();
        /*for ( $i = 0; $i<20;$i++){
            \App\Models\CuentaBancaria::create([
                'fecha_apertura' => '2020-02-02',
                'fecha_consulta' => '2020-02-02 18:30:20',
                'saldo' => '2000.00',
            ]);
        }*/
    }
}

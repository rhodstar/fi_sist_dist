<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class,20)->create();

        App\User::create([
            'name' => 'Rodrigo',
            'email' => 'rodrigo@example.com',
            'password' => Hash::make('banco123#')
        ]);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCuentaBancariaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_cuenta_bancaria', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('cuenta_bancaria_id');
            $table->timestamps();

            /*$table->foreign('id')
                ->references('user_id')
                ->on('users');
            $table->foreign('id')
                ->references('cuenta_bancaria_id')
                ->on('cuenta_bancaria');
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_cuenta_bancaria');
    }
}

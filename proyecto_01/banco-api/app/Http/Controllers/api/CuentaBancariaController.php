<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\CuentaBancaria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CuentaBancariaController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    public function showSaldo()
    {
        $cuentas = Auth::user()->cuentasBancarias;
        foreach ($cuentas as $cuenta){
            $cuenta->fecha_consulta = date('Y-m-d H:i:s');
        }
        return $this->sendResponse($cuentas, 'User bank accounts');
    }

    public function updateSaldo(Request $request){
        $input = $request->all();
        $cuenta_id = $input['id'];
        $cuenta = CuentaBancaria::findOrFail($cuenta_id);
        $cuenta->fecha_consulta = date('Y-m-d H:i:s');
        $cuenta->save();
        return $this->sendResponse($cuenta, 'Consulting Date updated.');
    }

}

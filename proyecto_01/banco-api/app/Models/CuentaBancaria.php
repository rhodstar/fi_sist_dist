<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuentaBancaria extends Model
{
    protected $table = 'cuenta_bancaria';
    protected $primaryKey = 'cuenta_bancaria_id';

    public function usuarios(){
        return $this->belongsToMany('App\User','user_cuenta_bancaria','cuenta_bancaria_id','user_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCuentaBancaria extends Model
{
    protected $table = 'user_cuenta_bancaria';
}

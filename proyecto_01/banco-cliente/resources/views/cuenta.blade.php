@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$nombre}}</h3>
                <h4>{{$email}}</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            @foreach($data as $cuenta)
                <div class="col-md-6 mb-3">
                    <div class="card h-100">
                        <div class="card-body">
                            <h5 class="card-title">Cuenta bancaria {{$loop->iteration}}</h5>
                            <p class="card-text"><strong>Cuenta ID: </strong>{{$cuenta->cuenta_bancaria_id}}</p>
                            <p class="card-text"><strong>Saldo: </strong>${{$cuenta->saldo}}</p>
                            <p class="card-text"><strong>Fecha apertura: </strong>{{$cuenta->fecha_apertura}}</p>
                            <p class="card-text"><small class="text-muted">Última consulta {{$cuenta->fecha_consulta}}</small></p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row justify-content-center">
        <a href="{{route('welcome')}}" class="btn btn-primary">Salir</a>
    </div>
@endsection

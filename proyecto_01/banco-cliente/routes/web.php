<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('cliente/saldo',function (Request $request){

    $input =$request->all();

    $client = new GuzzleHttp\Client();
    $response = $client->request('POST','http://127.0.0.1:8000/api/login',[
        'form_params' => [
            'email' => $input['email'],
            'password' => $input['password']
        ]
    ]);

    $data = json_decode($response->getBody());

    if($data->success){
        $accessToken =$data->data->token;
    }else{
        return redirect(route('welcome'))->withInput();
    }

    $response2 = $client->request('GET', 'http://127.0.0.1:8000/api/user/cuenta/consultar', [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$accessToken,
        ]
    ]);

    $data2 = json_decode($response2->getBody());

    foreach ($data2->data as $cuenta){
        $response3 = $client->request('POST','http://127.0.0.1:8000/api/user/cuenta/actualizar',[
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$accessToken,
            ],
            'form_params' => [
                'id' => $cuenta->cuenta_bancaria_id
            ]
        ]);
    }

    //$data3 = json_decode($response3->getBody());

    return view('cuenta')
        ->with('nombre',$data->data->name)
        ->with('email',$data->data->email)
        ->with('data',$data2->data);

})->name('cliente.saldo');

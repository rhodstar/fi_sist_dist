# Sistemas distribuidos 
## Proyecto  1

El siguiente proyecto consta de varias partes, y puede ser implementado a través de máquinas
virtuales.

* **Parte 1:**
  Realice la instalación del manejador de bases de datos mariaDB en un entorno linux y cree una
  base de datos que se encargará del almacenamiento de los datos de las cuentas de un banco.
  El diseño de dicha base se deja a consideración del alumno.
* **Parte 2:**
  Realice un API REST que permita la manipulación de la base de datos. El lenguaje para
  implementar dicha API se deja a consideración del alumno.
* **Parte 3:**
  Cree una aplicación que simule un cajero en el cual solo se puede realizar la lectura de los
  datos de los cuentahabientes del banco. Se debe poder conectar mas de un cliente a la vez.
  Dos clientes diferentes pueden consultar información de la misma cuenta, por tanto debe
  existir un registro de la última vista de los datos.

